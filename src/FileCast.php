<?php

namespace Klevlb\Filecast;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FileCast implements CastsAttributes
{
    public function __construct(protected string $path)
    {
    }

    /**
     * Cast the given value.
     *
     * @param  array<string, mixed>  $attributes
     */
    public function get(Model $model, string $key, mixed $value, array $attributes): mixed
    {
        if (Str::contains($value, 'https://') || Str::contains($value, 'http://')) {
            return $value;
        }

        if ($value === null) {
            return null;
        }

        $diskPath = $this->path;

        if (config('filesystems.default') === 's3') {
            $diskPath = 's3';
        }

        return Storage::disk($diskPath)->url($value);
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  array<string, mixed>  $attributes
     */
    public function set(Model $model, string $key, mixed $value, array $attributes): mixed
    {
        if (Str::contains($value, 'https://') || Str::contains($value, 'http://')) {
            return $value;
        }

        $diskPath = $this->path;

        $fileName = Storage::disk($diskPath)->put('', $value);

        if (config('app.env') === 'production') {
            FileUploadJob::dispatch(Storage::disk($diskPath)->path($fileName), $fileName, $this->path);
            $fileName = str_replace(['.jpg', '.jpeg', '.png', '.avif', '.apng', '.svg'], '.webp', $fileName);
        }

        if (config('filesystems.default') === 's3') {
            return 'public/' . $this->path . '/' . $fileName;
        }

        return $fileName;
    }
}
