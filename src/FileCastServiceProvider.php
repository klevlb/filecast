<?php

namespace Klevlb\Filecast;

use Illuminate\Support\ServiceProvider;

class FileCastServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/FileCast.php' => base_path('app/Casts')
        ]);
    }

    public function register()
    {
//        $this->app->make('Klevlb\Filecast\FileCast');
    }
}
