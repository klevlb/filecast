<?php

namespace Klevlb\Filecast;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;


class FileUploadJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(protected string $filePath, protected string $fileName, protected string $folderName)
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        try {
            ini_set('memory_limit', '3000M');

            [$this->fileName, $newPath] = str_replace(['.jpg', '.jpeg', '.png', '.avif', '.apng', '.svg'], '.webp', [$this->fileName, $this->filePath]);

            $manager = new \Intervention\Image\ImageManager(['driver' => 'imagick']);
            $manager->make($this->filePath)->save($newPath, 40);

            if (config('filesystems.default') === 's3') {
                Storage::disk('s3')->put('public/' . $this->folderName . '/' . $this->fileName, file_get_contents($newPath));
                File::delete($newPath);
            }

            File::delete($this->filePath);

        } catch (Exception $e) {
            Log::error($e);
        }
    }

    public function uniqueId()
    {
        return $this->filePath;
    }
}
