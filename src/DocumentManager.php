<?php

namespace Klevlb\Filecast;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Imagick;
use ImagickException;
use PhpOffice\PhpWord\Exception\Exception;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;

class DocumentManager
{
    public int $pdfPage = 0;
    public string $defaultFolderIcon = __DIR__ . '/folderIcon.png';

    public function __construct(public string $disk, public string|null $customPath = null)
    {
    }

    /**
     * @throws ImagickException
     * @throws Exception
     */
    public function generateThumbnail($file): array
    {
        $fileOriginalName = $file->getClientOriginalName();
        $fileExtension = $file->getClientOriginalExtension();

        $savedPhoto = Storage::disk($this->disk)->putFileAs($this->customPath ?? '', $file, $fileOriginalName);

        $fileExtension = strtolower($fileExtension);

        $diskPath = Storage::disk($this->disk)->path('');

        switch ($fileExtension) {
            case 'pdf':
                $thumbnail = $this->generateThumbnailFromPDF($diskPath, $savedPhoto);
                break;
            case 'docx':
                $thumbnail = $this->generateThumbnailFromDoc($file, $diskPath);
                break;
            case 'jpeg':
            case 'jpg':
            case 'png':
            case 'webp':
                $thumbnail = $savedPhoto;
                break;
            default:
                $thumbnail = $this->defaultFolderIcon;
        }

        return [
            'imageName' => $savedPhoto,
            'thumbnail' => $thumbnail,
            'fileOriginalName' => $fileOriginalName,
        ];
    }

    /**
     * @throws ImagickException
     */
    private function generateThumbnailFromPDF($path, $file)
    {
        $imagick = new Imagick();

        $imagick->readImage($path . '/' . $file . "[$this->pdfPage]");
        $imagick->setBackgroundColor('white');
        $imagick->setCompression(Imagick::COMPRESSION_JPEG);
        $imagick->setCompressionQuality(100);
        $imagick->setImageFormat("jpeg");
        $imagick->setImageAlphaChannel(Imagick::VIRTUALPIXELMETHOD_WHITE);
        $imageName = Str::random() . rand(1000, 9999);
        $imagick->writeImages($path . '/' . $imageName . '.jpg', false);

        return $imageName . '.jpg';
    }

    /**
     * @throws ImagickException
     * @throws Exception
     */
    private function generateThumbnailFromDoc($file, $diskPath): string
    {
        if (!File::exists(public_path('storage/tempDocuments'))) {
            File::makeDirectory(public_path('storage/tempDocuments'), 0755, true);
        }

        $pdfLocation = base_path('vendor/dompdf/dompdf');

        Settings::setPdfRendererPath($pdfLocation);
        Settings::setPdfRendererName('DomPDF');

        $word = IOFactory::load($file);

        $wordToPdf = IOFactory::createWriter($word, 'PDF');

        $imageName = Str::random() . rand(1000, 9999);

        //Save the generated pdf
        $wordToPdf->save(public_path('storage/tempDocuments/' . $imageName . '.pdf'));

        $imagick = new Imagick();
        $imagick->readImage(public_path('storage/tempDocuments/'. $imageName . ".pdf[$this->pdfPage]"));
        $imagick->setResolution(300, 300);
        $imagick->scaleImage(500, 500, true);

        $imagick->setImageAlphaChannel(Imagick::VIRTUALPIXELMETHOD_WHITE);
        $newImage = Str::random() . rand(1000, 9999);
        $imagick->writeImages($diskPath . '/' . $newImage . '.jpg', false);

        //Delete the generated pdf
        File::delete(public_path('storage/tempDocuments/' . $imageName . '.pdf'));

        return $newImage . '.jpg';
    }

    public function setPdfPage($pageNumber)
    {
        $this->pdfPage = $pageNumber;

        return $this;
    }

    public function setDefaultFolderIcon($iconPath)
    {
        $this->defaultFolderIcon = $iconPath;

        return $this;
    }
}
